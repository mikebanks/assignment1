﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Assignment1_MikeBanks.Models
{
    //class for team information
    public class TeamInformation
    {
        //all five of the views can be seen below, and all of them are required (displaying appropriate error message if left empty by the user)
        
        //makes the field required
        [Required(ErrorMessage = "Please enter the team's sport (ie. Hockey)")]
        public string TeamSport { get; set; }

        [Required(ErrorMessage = "Please enter the team's league (ie. NHL)")]
        public string TeamLeague { get; set; }

        [Required(ErrorMessage = "Please enter the team's city (ie. Toronto)")]
        public string TeamCity { get; set; }

        [Required(ErrorMessage = "Please enter the team's name (ie. Maple Leafs)")]
        public string TeamName { get; set; }

        [Required(ErrorMessage = "Please enter your name (ie. John Smith)")]
        public string Name { get; set; }

    }
}