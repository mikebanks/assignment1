﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment1_MikeBanks.Models;

namespace Assignment1_MikeBanks.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home - default landing page - displays welcome message and button for form to user
        public ActionResult Index()
        {
            ViewBag.Welcome = "Hello User, Welcome!";
            return View();
        }

        //Action method that takes no parameters, loads the favorite team form
        [HttpGet]
        public ActionResult FavoriteTeamForm()
        {
            return View();
        }

        //Action method that takes the information entered into the form as a parameter
        [HttpPost]
        public ActionResult FavoriteTeamForm(TeamInformation teamInfo)
        {
            //if all of the fields were entered correctly
            if (ModelState.IsValid)
            {
                //load the 'thanks' view
                return View("ThanksForTeamInfo", teamInfo);
         
            } else
            {
                //if information not entered correctly. reload the form view with the error messages
                return View();
            }

        }

        //Action method that takes all of the fields as parameters in the URL (to avoid using the form) - no GET/POST used - not secure
        public ActionResult ThanksForTeamInfo(string sport, string league, string city, string teamName, string name)
        {
            //create a new TeamInformation object
            TeamInformation teamInfo = new TeamInformation
            {
                //set all of the fields entered through the 
                TeamSport = sport,
                TeamLeague = league,
                TeamCity = city,
                TeamName = teamName,
                Name = name
            };

            //load the 'thanks' view
            return View("ThanksForTeamInfo", teamInfo);
        }
    }
}